//This method adds a slot for the doctor
 addSlot = () => 
{
    const date = document.getElementById("date").value;
    const time = document.getElementById("time").value;
    const specialist = document.getElementById("specialist").value;

    let httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }
 // api call response handling 
    httpReq.onreadystatechange =function () 
    {
            if(this.readyState===4 && this.status===201)
            {
                    alert("Slot added successfully");
                    window.location.assign("success.html");
                
            }
    } 

    let obj = {doctoremail:sessionStorage.getItem('${user}'),date,time,specialist,booked:'no'};

    console.log(obj);
 /* api call to slots */
     httpReq.open('POST','http://localhost:3000/slots',true);
     httpReq.setRequestHeader("Content-type","application/json");
     httpReq.send(JSON.stringify(obj));
 


}
//This method checks for the existance of the slot for the doctor
 checkAvailability= () =>
{
    const date = document.getElementById("date").value;
    const time = document.getElementById("time").value;
    const specialist = document.getElementById("specialist").value;
    const email = sessionStorage.getItem('user');

    //Validation starts here
    let curDate = new Date();
    if(date===null||!date.length>0){
         alert("Please Provide date for booking slot ");
    return false;
    }
    else if (new Date(date).getTime() <= curDate.getTime()) {
         alert("The Date must be equal or later than the current date");
    return false;
    }
    
    if(time===null||!time.length>0){
    alert("Please Provide time for booking slot");
    return false;
    }
  
		


    let httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest();
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }
 // api call response handling 
    httpReq.onreadystatechange = function()
    {
            if(this.readyState===4 && this.status===200)
            {
                let data = JSON.parse(this.response);
                const len = data.length;
                if(len>0)
                {
                    alert("Slot already available for the given Date,Time and Specialization.");
                    return false;
                }
                else
                   addSlot();
            }
    } 

  
//string literals
    httpReq.open('GET','http://localhost:3000/slots?date=${date}&time=${time}&specialist=${specialist}&doctoremail=${email}');
    httpReq.send();



}
