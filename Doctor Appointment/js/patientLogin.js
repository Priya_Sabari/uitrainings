showAvailableSlots();
//This function shows available slots for all doctors
 showAvailableSlots = () =>
{
    let httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }
  // api call response handling 
    httpReq.onreadystatechange = function() 
    {
  if(this.readyState===4 && this.status===200)
  {
      //Create Heading
    const appointmentHeading =  document.createElement("h2");
    appointmentHeading.appendChild(document.createTextNode("Slots Available for booking"));
    appointmentHeading.setAttribute("id",'headingID');

    const table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      const thead = document.createElement('thead');
      const tbody = document.createElement('tbody');

      const headTr = document.createElement('tr');

      const td1 = document.createElement('td');
      const td1Text = document.createTextNode("Doctor Mail ID");
      td1.appendChild(td1Text);

      const td2 = document.createElement('td');
      const td2Text = document.createTextNode("Date");
      td2.appendChild(td2Text);

      const td3 = document.createElement('td');
      const td3Text = document.createTextNode("Time");
      td3.appendChild(td3Text);

      const td4 = document.createElement('td');
      const td4Text = document.createTextNode("Specialist");
      td4.appendChild(td4Text);

      const td5 = document.createElement('td');
      const td5Text = document.createTextNode("Mention Illness");
      td5.appendChild(td5Text);

      const td6 = document.createElement('td');
      const td6Text = document.createTextNode("Action");
      td6.appendChild(td6Text);

     
      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
      headTr.appendChild(td6);
   

      thead.appendChild(headTr);
 
       let data = JSON.parse(this.response);
       const len = data.length;
      if(len>0)
      {
       for(let i=0;i<len;i++)
       {            
        const tbodyTr = document.createElement("tr");
        const td1 = document.createElement("td");
        const td2 = document.createElement("td");
        const td3 = document.createElement("td");
        const td4 = document.createElement("td");
        const td5 = document.createElement("td");
        const td6 = document.createElement("td");

           let slotId = data[i].id;
           let td1TextNode = document.createTextNode(data[i].doctoremail);
           let td2TextNode = document.createTextNode(data[i].date);
           let td3TextNode = document.createTextNode(data[i].time);
           let td4TextNode = document.createTextNode(data[i].specialist);
           let td5InputElement = document.createElement('${input}');
           td5InputElement.setAttribute("type","text");
           td5InputElement.setAttribute("id","res");

       const bookAppointmentButton = document.createElement("button");
       const bookAppointmentButtonTextNode = document.createTextNode("Book Appointment");
       bookAppointmentButton.appendChild(bookAppointmentButtonTextNode);
       bookAppointmentButton.addEventListener('click',() =>
       {
        const reason = document.getElementById('${res}').value;
           if(reason.trim().length<1||reason==="" || reason===null){
                alert("Please Enter reason for booking");
            return false;       
            }
            let httpReq1;
            if(window.XMLHttpRequest)
            {
                httpReq1 = new XMLHttpRequest
            }
            else{
                httpReq1  = new ActiveXObject("Microsoft.XMLHTTP");
            }

           let data = this.parentElement.parentElement.cells;
           console.log(data);
           console.log(slotId);
         
           let reason = data[4].childNodes[0].value;
        
           let obj2 = {doctoremail:data[0].innerHTML,date:data[1].innerHTML,time:data[2].innerHTML,specialist:data[3].innerHTML,reason:reason,patientemail:sessionStorage.getItem('user')};
          
            // api call response handling 
           httpReq1.onreadystatechange = function() 
            {
                if(this.readyState===4 && this.status===201)
                {
                    let httpReq2;
                    if(window.XMLHttpRequest)
                    {
                        httpReq2 = new XMLHttpRequest
                    }
                    else{
                        httpReq2  = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    

                    let obj3 =  {doctoremail:data[0].innerHTML,date:data[1].innerHTML,time:data[2].innerHTML,specialist:data[3].innerHTML,booked:"yes"};
                    console.log(slotId);
                    console.log(obj3);
                      // api call response handling 
                    httpReq2.onreadystatechange = function() 
                    {
                        if(this.readyState===4 && this.status===200)
                        {
                            window.location.assign("bookAppointment.html");
                        }
                    }
                    //string literal
                    httpReq2.open('PUT','http://localhost:3000/slots?slotId=${slotId}');
                    httpReq2.setRequestHeader("Content-type","application/json");
                    httpReq2.send(JSON.stringify(obj3));
                    
                }
            }
         /* api call to appointment */
            httpReq1.open('POST','http://localhost:3000/appointment',false);
            httpReq1.setRequestHeader("Content-type","application/json");
            httpReq1.send(JSON.stringify(obj2));
           
       })
      
           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5InputElement);
           td6.appendChild(bookAppointmentButton);       

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);
           tbodyTr.appendChild(td6);

           tbody.appendChild(tbodyTr);        

       }
      }
      else{
            const data = document.createElement("h4");
            const noData = document.createTextNode("No Data Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      const body = document.getElementsByTagName('body')[0];
      
      body.appendChild(appointmentHeading);
      body.appendChild(table);
  }
}
//string literal
httpReq.open('GET','http://localhost:3000/slots?booked=${no}');
httpReq.send();
}
